// 首页的滚动条显现
function scroll() {

	let oHeader = document.querySelector("header");
	let oOneTop = document.querySelector("#top");
	window.onscroll = function() {
		//兼容性:获取滚动条距顶端的距离
		var _top = document.body.scrollTop || document.documentElement.scrollTop;

		if (_top > 100) {
			oHeader.style.display = "block";
			oHeader.style.position = "sticky";
			oHeader.style.top = 0;
			oOneTop.style.display = "none";
		} else if (_top <= 100) {
			oHeader.style.display = "none";
			oOneTop.style.display = "block";
		}
	}
}
// ------------------------------------------------------------------------------------
//首页轮播图的显现
class Banner {
	constructor(newImg, newLis) {
		// 定义全局变量
		this.index = 0;
		this.oImg = newImg;
		this.oLis = newLis;
		this.oImg.style.backgroundImage = "url(imgs/" + this.index + ".jpg)";
		
	}

	// 设置背景
	setBgcImage() {
		this.oImg.style.backgroundImage = "url(imgs/" + this.index + ".jpg)";
	}

	// 设置li颜色
	setLiColor() {
		// 遍历每一个li
		for (let i = 0; i < oLis.length; i++) {
			// 没有事件影响this，不用添加that变量
			if (this.index == i) {
				this.oLis[i].style.width = "5px";
				this.oLis[i].style.height = "5px";
				this.oLis[i].style.border = "1px solid white";
				this.oLis[i].style.borderRadius = "50%";
				this.oLis[i].style.background="rgba(0,0,0,0)";
				// this.oLis[i].style.backgroundPositionY="10px";
			} else {
				this.oLis[i].style.width = "2px";
				this.oLis[i].style.height = "2px";
				this.oLis[i].style.border = "1px solid white";
				this.oLis[i].style.borderRadius = "50%";
				this.oLis[i].style.backgroundColor="white";
			}
		}
	}

	// 下一个事件
	next() {
		this.index++;

		// 当下标加到5的时候，等于li的长度，让下标回到0
		if (this.index == 5) {
			this.index = 0;
		}

		this.setBgcImage();
		this.setLiColor();
	}

	// 上一个事件
	first() {
		this.index--;

		if (this.index == -1) {
			// 当下标== -1的时候，就让index等于第五个图，下标等于5-1，下标就是4，也就是第五个图片
			this.index = oLis.length - 1; //代表下标
		}

		this.setBgcImage();
		this.setLiColor();
	}

	// 点击li事件
	click() {
		let that = this;
		for (let i = 0; i < oLis.length; i++) {
			oLis[i].onclick = function() {
				that.index = i;
				that.setBgcImage();
				that.setLiColor();
			}
			
		}
	}


	// 绑定事件
	bingEvent() {
		let oBtnFirst = document.querySelector("#btn-first");
		let oBtnNext = document.getElementById("btn-next");
		// 这块有绑定事件，所以要用that
		let that = this;

		oBtnFirst.onclick = function() {
			that.first();
		}
		oBtnNext.onclick = function() {
			that.next();
		}

		this.click();
	}


	// 加定时器
	change() {
		let that = this;
		let start = function() {

			if (that.index < oLis.length - 1) {
				that.index++;
			} else {
				that.index = 0;
			}
			that.setBgcImage();
			that.setLiColor();

		}

		let time = setInterval(start, 2000);
		oDiv.onmouseout = function() {
			time = setInterval(start, 2000);
		}
		oDiv.onmouseover = function() {
			clearInterval(time);
		}
	}

}


// 返回顶部

function oBack(){
	$(".back-top").click(function(){
		$(window).scrollTop(0);
	})
}
	
	

	