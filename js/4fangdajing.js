
    class Magnifier {
        constructor(newsmallbox, newmask, newbigbox,newloupe) {
            this.small = newsmallbox;
            this.mask = newmask;
            this.big = newbigbox;
			this.loupe=newloupe;
        }

        // 成员方法
        mouseover() {
            let that = this;
            this.small.onmouseover = function() {
                that.mask.style.display = "block";
                that.big.style.display = "block";
            }
        }

        mouseout() {
                let that = this;
                this.small.onmouseout = function() {
                    that.mask.style.display = "none";
                    that.big.style.display = "none";
                }
            }
            // 鼠标在小盒子上面移动
        mousemove() {
            let that = this;
            this.small.onmousemove = function(evt) {
                let e = evt || event;
                // 是罩层距离小图的左边的位置，e.page获取的是鼠标在小图上距离整个大白板的距离，
				// 要减去这个小图距离最左边的距离,而offsetleft只是说该元素距离最近父元素左边的距离
				// 剩下的就是罩层在小图中的距离
				
                // 求罩层距离小图左边的位置，！！！当 减去small.offsetLeft时，只能写this.offsetLeft,因为this就代表了当前事件的元素，或者写that.small.offsetLeft,不然是错误的
				
				// 208.3是我在控制台中量出小图片距离最左边的距离
				// 249 是在控制台中右上角可以看到小盒子距离最上边的位置
                let left = e.pageX - that.loupe.offsetLeft - that.mask.offsetWidth / 2;
                // 求罩层距离小图上面的距离 ,减去父元素的高度
                let top = e.pageY - that.loupe.offsetTop - that.mask.offsetHeight / 2;



                // 设置边界
                if (left < 0) {
                    left = 0;
                }

                let maxLeft = this.offsetWidth - that.mask.offsetWidth;

                if (left > maxLeft) {
                    left = maxLeft;
                }

                if (top < 0) {
                    top = 0;
                }

                let maxTop = this.offsetHeight - that.mask.offsetHeight;

                if (top > maxTop) {
                    top = maxTop;
                }
                // 这个最终距离左边和上边的值，必须放在判断边界条件之后
                that.mask.style.left = left + "px";
                that.mask.style.top = top + "px";
                // 获取大图放大镜的背景图位置
                //比例尺
                //小图片:大图片 = 小窗口:大窗口
                //that.mask.offsetWidth*x = left * that.bigBox.offsetWidth;
                let x = left * that.big.offsetWidth / that.mask.offsetWidth;
                let y = top * that.big.offsetHeight / that.mask.offsetHeight;



                // 背景图片的定位，负号，反方向移动
                that.big.style.backgroundPositionX = -x + "px";
                that.big.style.backgroundPositionY = -y + "px";
            }
        }

    }



   