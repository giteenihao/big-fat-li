let ss1 = sessionStorage;
// 获取存储的num值
let num = ss1.getItem("num");



// 当全选按钮被点击，剩下的全部点击
$("#in1").click(function(){
	// 这种方法只能点击一次
	// $("input").attr("checked","checked");
	
	// 这种方法是点击一次，判断这个框有没有被选中，被选中了，则下面的所有框都被选中，若没有被选中，则所有的input框都不被选中
	 if ($(this).prop("checked")) {
	        $("input").attr("checked", true);
	    } else {
	        $("input").attr("checked", false);
	    }
})