class Pop {
	constructor(newoBox) {
		this.box = newoBox;
		// 这块已经是定义了全局变量，可以作用于其他地方；
		this.btnClose = null;
	}

	// 设置弹出框的位置
	setPosition() {
		// 所有未被显示的元素，其offset相关属性都为0;  所以要将元素显示，要写在用offse属性之前
		this.box.style.display = "block";
		this.box.style.left = window.innerWidth / 2 - this.box.offsetWidth / 2 + "px";
		this.box.style.top = window.innerHeight / 2 - this.box.offsetHeight / 2 + "px";

		// 在设置box的位置时，调用创建关闭按钮的方法
		// this.creatBtnClose();
	}


	// 创建一个关闭的按钮
	creatBtnClose() {
		// 判断条件，没有这个关闭按钮的时候，进行创建
		if (this.btnClose == null) {
			// 创建按钮，不用写let，
			// 要是写了let ，作用在这个作用域中的就不用加this了
			this.btnClose = document.createElement("input");

			// 重点:要先绑定到父元素身上，offset属性才回有值
			this.box.appendChild(this.btnClose);
			this.btnClose.value = "X";
			this.btnClose.style.width = "30px";
			this.btnClose.style.height = "30px";
			// 一定记得加定位才能移动元素
			this.btnClose.style.position = "absolute";
			// this.btnClose.style.right = "0";
			this.btnClose.style.left = this.box.offsetWidth + "px";
			this.btnClose.style.top = 0;
			this.btnClose.style.backgroundColor = "rgba(0,0,0,0)";
			this.btnClose.style.color = "white";
			this.btnClose.style.textAlign="center";
			this.btnClose.style.border = "none";
			this.btnClose.style.cursor = "pointer"
		}
	}


	close() {
		let that = this;

		this.btnClose.onclick = function() {
			that.box.style.display = "none";
		}
	}

	bind() {
		this.setPosition();
		this.creatBtnClose();
		this.close();
	}
}




		


