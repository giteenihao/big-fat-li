// 首页的滚动条显现
function scroll() {

	let oHeader = document.querySelector("header");
	let oOneTop = document.querySelector("#top");
	window.onscroll = function() {
		//兼容性:获取滚动条距顶端的距离
		var _top = document.body.scrollTop || document.documentElement.scrollTop;

		if (_top > 80) {
			oHeader.style.display = "block";
			oHeader.style.position = "sticky";
			oHeader.style.top = 0;
			oOneTop.style.display = "none";
		} else if (_top <= 80) {
			oHeader.style.display = "none";
			oOneTop.style.display = "block";
		}
	}
}

// 错误

	// $("#btn").click(function(){
	// 	$(window).scrollTop(0);
	// })
	
	
	// 错误写法
	// function scroll(){
	// 	if($(window).scrollTop()>=80){
	// 		$("header").css({
	// 			display:"block",
	// 			position:"fixed",
	// 			top:0
	// 		});
	// 		$("#top").css({
	// 			display:"none"
	// 		});
	// 	}else if($(window).scrollTop()<80){
	// 		$("header").css({
	// 			display:"none"
	// 		});
	// 		$("#top").css({
	// 			display:"block"
	// 		});
	// 	}
	// }
	