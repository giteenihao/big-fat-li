// // 获取上个页面存储的下标，这个下标中存放了有关的所有数据
// // 当每点击列表单的时候，会进入相对应下标的那一列数据
// // sessionStorage.getItem("listindex");
// let ss = sessionStorage;  
// let list_index = ss.getItem("listindex");




// $.get("./php/4fangdajing.php",{
// 	listinfo:list_index //把获取的上个页面session的键值在这个页面进行传参
// },function(resText,status){
// 	// 先打印康康返回来的数据类型
// 		// console.log(resText);//字符串
// 		// 字符串拆分成数组
// 		let arr = resText.split("#");
// 		// console.log(arr[0]);

// 		// let loupearr;
// 	// for(let i = 0;i<arr.length;i++){
// 		// 每一条数组里面的内容都拆分开
// 		// loupearr = arr[i].split("#");
// 		// console.log(loupearr);  //一共打印了六条数组，最后一个是空数组
// 		// for(let j = 0;j<loupearr.length;j++){
// 			$(".smallbox").css({
// 				backgroundImage:"url(imgs/list-"+Number(arr[0])+".jpg)"
// 			})
// 			$(".bigbox").css({
// 				backgroundImage:"url(imgs/list-"+Number(arr[0])+".jpg)"
// 			})

// 			// $(".picture").attr("src","url(imgs/"+loupearr[0]+".jpg)");
// 			// $(".picture1").attr("src","url(imgs/"+loupearr[0]+".jpg)");

// 			$(".ename").html(arr[1]);
// 			$(".cname").html(arr[2]);
// 			$("#money .price").html(arr[3]);
// 			// 查找下拉框内容的方法，查select下的option
// 			$(".counts").find("option:selected").text(arr[4]);

// 		// }



// 	// }




// 	// 点击时出现黄色边框
// 	// $(".blockall").click(function() {
// 	// 	$(this).css({
// 	// 		border: "2px solid gold",
// 	// 		// transform: "scale(1.5,1.5)",//缩放效果
// 	// 		transform: "translateY(-4px)"  //图片向上移效果
// 	// 	}).siblings(this).css({
// 	// 		border: "none",
// 	// 		transform: "translateY(0)"  //让其他的位置在原地不动
// 	// 	})
// 	// })

// 	// siblings是兄弟节点


// 	// let that = $(this);
// 	// 点击时，让上面的图变成下面的图
// 	// $(".blockall").eq(1).click(function(){
// 	// 	$(".smallbox").css({
// 	// 		backgroundImage:"url(imgs/detail-1.jpg)"
// 	// 	})
// 	// 	$(".bigbox").css({
// 	// 		backgroundImage:"url(imgs/detail-1.jpg)"
// 	// 	})
// 	// })
// })



// 正解----------------------------------------------------------------------------------------------------------------
// 获取上个页面存储的session
let ss = sessionStorage;
let list_index = ss.getItem("listindex");

$.get("./php/4fangdajing.php", {
	// 将获取到的index下标值，赋给listinfo,传参给后台，后台查找这个相对应的那一条数据
	index: list_index
}, function(resText, status) {
	// 检查打印返回来的值
	// console.log(resText);  //就只是一条数据(字符串)
	let arr = resText.split("#"); //把字符串按#分割成数组
	//1. 对象打点+属性，获取的就是数组里面的值arr[0],不需要遍历了
	//2.点击上一个页面，获取下标，下标值和图片id名是相同的
	//3.点击列表页的哪个图片，放大镜页面跳转的图片与列表页的图片相同


	//1.1------放大镜设置的图
	$(".smallbox").css({
		backgroundImage: "url(imgs/list-" + Number(arr[0]) + ".jpg)"
	});
	$(".bigbox").css({
		backgroundImage: "url(imgs/list-" + Number(arr[0]) + ".jpg)"
	});



	// 1.2------放大镜下的图  ,把图库中的名字都格式化命名，都按下标查找图
	$(".picture").attr("src", "imgs/detail" + arr[0] + "-" + 1 + ".jpg")
	$(".picture1").attr("src", "imgs/detail" + arr[0] + "-" + 2 + ".jpg")
	$(".picture2").attr("src", "imgs/detail" + arr[0] + "-" + 3 + ".jpg")



	// 1.3------文本框获取的内容
	$(".ename").html(arr[1]);
	$(".cname").html(arr[2]);
	$("#money .price").html(arr[3]);
	// 查找下拉框内容的方法，查select下的option
	$(".counts").find("option:selected").text(arr[4]);




	// 2.1  ---当滑动到每一个相同类名的小图时，当前的图片出现的样式
	$(".blockall").mouseover(function() {
		$(this).css({
			border: "3px solid gold",
			translateY: "-4px"
		}).siblings(this).css({
			border: "none",
			translateY: "0"
		})
	})

	// 这是一种方法，加.siblings()也是一种方法
	// $(".blockall").mouseout(function(){
	// 	$(this).css({
	// 		border:"none",
	// 		translateY:"0"
	// 	})
	// })





	// 2.2  -----点击下面的每一个图时，让他换到小图中
		
	$(".p-img").click(function(){
		// 获取当前的点击时的图片路径
	let att = $(this).attr("src");
		// console.log(att)
		// 赋给小图的路径，用attr
		$(".smallbox").css({
			// 模板字符串，` ${变量} `
			backgroundImage:`url(${att})`
		})
		$(".bigbox").css({
			// 模板字符串，` ${变量} `
			backgroundImage:`url(${att})`
		})
	})




})
