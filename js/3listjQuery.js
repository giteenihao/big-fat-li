// 列表页已经不需要传数据了，只需要从后台获取数据

$.get("./php/3.2list.php", function(resText, status) {

	// console.log(resText); //打印出来了四条结果集

	// 把后台传回来的多条字符串先按";"拆分成数组
	let arr = resText.split(";");
	// console.log(arr); //四条数组

	// // 这样拆分不了，不确定是哪个arr
	// // let listarr = arr.split("#");

	let listarr = [];

	// 有五个数组，多了一个空的，不需要的，减去1，就刚好是获取的四个

	for (let i = 0; i < arr.length - 1; i++) {
		// 要拆分每一条数组，共四条数组,i=1时，拆分第一条数组,把拆分好的数组插入到listarr中
		listarr.push(arr[i].split("#"))
	}


	// 这个只用在这里克隆前端写的一个模板样式，有几条数据，就会克隆几个模板
	listarr.forEach(item => { //item是listarr数组里面的每一项内容
		let newsize = $("#size").clone(true);
		newsize.attr("id", ""); //清空id，清空的是样式模板中id的内容(隐藏属性)
		$("#content-right-con").append(newsize); //把克隆的模板跟父元素绑定
		// console.log(newsize.item[0]);
		// forEach里面似乎直接用不了$("*").html()
		// 查找当前模板下相对应名为.size-ptext的html值
		// find查找父级元素的子元素
		newsize.find($("img")).attr("src", "imgs/list-" + item[0] + ".jpg");
		newsize.find($(".size-ptext")).html(item[1]);
		newsize.find($(".size-pcname")).html(item[2]);
		newsize.find($(".size-pename")).html(item[3]);
		newsize.find($(".size-price")).html(item[4]);

	})
	console.log(listarr);


	// ----------------------------------------------------------------------------------------
	// 第二种方法:这个是要在页面中写好多模块
	// let arr = resText.split(";");
	// let listarr;
	// for (let i = 0; i < arr.length-1; i++) {
	// 		// 要拆分每一条数组，共四条数组,i=1时，拆分第一条数组
	// 		listarr = arr[i].split("#");
	// 		$("#imgid").attr("src", "imgs/" + listarr[0] + ".jpg");
	// 		$(".size-ptext").html(listarr[1]);
	// 		$(".size-pcname").html(listarr[2]);
	// 		$(".size-pename").html(listarr[3]);
	// 		$(".size-price").html(listarr[4]);
	// 	}

})


// 鼠标滑上出现蒙版
// 方法一:
$(".size").mouseover(function() {
	// 点击当前的事件，查找当前事件下的元素的样式
	$(this).find($(".size-mask")).css({
		display: "block"
	})
})
$(".size").mouseout(function() {
	// 点击当前的事件，查找当前事件下的元素的样式
	$(this).find($(".size-mask")).css({
		display: "none"
	})
})

// 方法二:
// 鼠标滑上
// $(".size").mousemove(function(){
// 	$(".size .size-mask").removeClass("active")//1  (显示属性)
// 	console.log($(this).find($(".size-mask"))) //2
// 	if(!$(this).find($(".size-mask")).hasClass("active")){//3
// 		$(this).find($(".size-mask")).addClass("active")
// 	}
// 	// $(".size .size-mask").addClass("active")
// })
// 点击哪个出现哪个,点击后，先清空所有的类名为active的，
// 当前事件下查找目标元素，
//判断条件，要是没有查找到这个类名，就给当前调用这个事件的函数添加这个类名的事件，点击哪个事件，就给哪个事件添加

// 鼠标滑出，直接清空添加的显示属性
// $(".size").mouseout(function(){
// 	$(".size .size-mask").removeClass("active");
// })



// 存储sessionstorage;,点击时，获取当前元素下标
let indexList;
$(".size").click(function() {
	// 获取当前的下标
	indexList = $(this).index();
	// console.log(indexList);//检查
	let ss = sessionStorage;  
	ss.setItem("listindex",indexList-1);
	// indexList下标的值是1，想让他从0开始，只需要-1
	// sessionStorage.setItem('listindex', indexList-1);
	// console.log(sessionStorage.getItem('listindex'));
	window.location.href="http://127.0.0.1/TwoProjectlisisi/4.fangdajing.html";
})
